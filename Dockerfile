FROM mhart/alpine-node:10

# Directory where the service will be installed
ARG API_DIR=/var/www/api
ARG PORT=5577
ENV PORT ${PORT:-5577}

# Prepare required directories
RUN mkdir -p ${API_DIR}

# Use app directory as current workdir
WORKDIR ${API_DIR}

# Copy artifact to workdir
COPY . ${API_DIR}/

# Expose Port
EXPOSE ${PORT}