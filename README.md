# Land of Here API
[![pipeline status](https://gitlab.com/land-of-here/land-of-here-api/badges/master/pipeline.svg)](https://gitlab.com/land-of-here/land-of-here-api/commits/master)
[![pipeline status](https://gitlab.com/land-of-here/land-of-here-api/badges/develop/pipeline.svg)](https://gitlab.com/land-of-here/land-of-here-api/commits/develop)


GraphQL API for Land of Here

## Development

### Quick start

Requires Docker + Docker-Compose

```
git clone https://gitlab.com/land-of-here/land-of-here-api.git

cd land-of-here-api

npm install

docker-compose -f prisma/docker-compose.yml up -d

cp .env.sample .env

npm run dev

```

### Dev documentation

Deverloper notes are being collected on the [GitLab Land of Here API wiki](https://gitlab.com/land-of-here/land-of-here-wiki/wikis/)