import * as request from 'supertest'
import { app } from '../../index'

describe("GET /graphql", () => {
 it("SHOULD return 400",  () => {
  request(app)
   .get("/graphql")
   .end((err, res) => {
      expect(res.status).toBe(400);
   });
 });
});