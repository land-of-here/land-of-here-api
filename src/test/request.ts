import { graphql } from 'graphql'
import { schema }  from '../index'
import { prismaMock } from './mocks'

type Options = {
  context?: {
    user?: Object,
  },
  variables?: Object,
};

// Nice little helper function for tests
export const request = (query: any, { context, variables }: Options = {}) => {
  return graphql(
    schema,
    query,
    undefined,
    {
      prismaMock,
      ...context,
    },
    variables
  );
};