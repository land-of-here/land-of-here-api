import { userMock } from './User'

const prismaMock = {
  user: jest.fn(() => (userMock)),
  users: jest.fn(() => ({ userMock })),
}

export const aggregateMock = jest.fn(() => ({
  count: 2,
}));

export { prismaMock, userMock }