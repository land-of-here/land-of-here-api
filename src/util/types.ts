import { Prisma } from '../../generate/prisma-client'

export interface Context {
  prisma: Prisma
  req: any
}