import * as bcrypt from 'bcryptjs'
import { hashedPassword, validatePassword, createToken, getUserId } from '../auth'

interface Context {
  prisma: any,
  req: any
}

describe('utility Auth', () => {

  test('creates JWT', async () => {
    const userId = '123'
    const token = await createToken(userId)
    expect(token).toEqual(expect.stringMatching(/^eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9/))
  })

  test('creates hashed password', async () => {
    const password = '123magicPassword'

    const hashed: any = await hashedPassword(password)
    const compared = await bcrypt.compare(password , hashed) 
    expect(compared ).toBe(true)
  })

  test('checks for valid password', async () => {
    const password = '123magicPassword'

    const hashed: any = await hashedPassword(password)
    const compared = await validatePassword(password , hashed) 
    expect(compared ).toBe(true)
  })

  test('gets userid from JWT', async () => {
    const userId = '123'
    const token = await createToken(userId)
    const mockCtx: Context = {
      prisma: {},
      req: {
        headers: {
          authorization: `Bearer ${token}`
        }
      }
    }
    const user = await getUserId(mockCtx)

    expect(user).toEqual(userId)
  })
})