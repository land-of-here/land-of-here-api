import 'dotenv/config'
import * as express from 'express'
import { join } from 'path'
import createApolloServer from './apollo-server'
import { makePrismaSchema } from 'nexus-prisma'
import { prisma } from '../generate/prisma-client'
import datamodelInfo from '../generate/nexus-prisma'

import { logger, formatError } from './util'

import { resolvers } from './resolvers'

const GRAPHQL_ENDPOINT = '/graphql'
const GRAPHQL_SUBSCRIPTIONS = '/graphql'
const PORT = process.env.PORT
const NODE_ENV = process.env.NODE_ENV

export const app = express()

app.post(GRAPHQL_ENDPOINT)

export const schema = makePrismaSchema({
  types: resolvers,

  prisma: {
    datamodelInfo,
    client: prisma
  },

  outputs: {
    schema: join(__dirname, '/generated/schema.graphql'),
    typegen: join(__dirname, '/generated/nexus.ts')
  }
})

export const server = createApolloServer(app, schema, {
  graphqlEndpoint: GRAPHQL_ENDPOINT,
  subscriptionsEndpoint: GRAPHQL_SUBSCRIPTIONS,
  apolloServerOptions: { formatError },
  context: (req):{} => ({
    ...req,
    token: req.headers ? req.headers : undefined,
    user: req.user ? req.user : undefined,
    userId: (req.headers && req.headers.userid) ? req.headers.userid : undefined,
    prisma: prisma
  })
})

server.listen({ port: PORT }, () => {
  logger.info(
    `GraphQL Server is running on http://localhost:${PORT}${GRAPHQL_ENDPOINT} in "${NODE_ENV}" mode\n`
  )
})
