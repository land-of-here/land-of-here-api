import { prismaObjectType } from 'nexus-prisma'

export const UserProfile = prismaObjectType({
  name: 'UserProfile',
  definition(t) {
    t.prismaFields(['*'])
  },
})