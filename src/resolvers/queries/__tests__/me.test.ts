import { me } from '../me'

const prismaContext = jest.fn()

const userPayload = {
  user: {
    username: 'bob',
    handle: 'bob',
    profile: { email: 'bob@email.com' }
  }
}

describe('query me', () => {
  test('expects context argument', async () => {
    const me = jest.fn()
    me(prismaContext)
    expect(me).toBeCalledWith(prismaContext)
  })

  test('returns object user', async () => {
    const me = jest.fn()
    me.mockReturnValue(userPayload);
    
    const user = await me(prismaContext)
    expect(user).toHaveProperty('user')
  })
})