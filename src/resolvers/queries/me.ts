import { getUserId } from '../../util/auth'

export const me = async (prismaContext: any) => {
  const userId = getUserId(prismaContext)
  return prismaContext.prisma.user({ id: userId })
}
