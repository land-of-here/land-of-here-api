import * as bcrypt from 'bcryptjs'
import { login } from '../login'
import { validatePassword, hashedPassword } from '../../../util/auth'

const username = 'usersarah'
const password = '123magicPassword'
const userContext = jest.fn()

const loginPayload = {
  token: '123',
  user: {
    username: 'usersarah',
    handle: 'usersarah',
    profile: { email: 'sarah@email.com' }
  }
}

describe('mutation login', () => {
  test('expects username, password', async () => {
    const register = jest.fn()
    register(username, password, userContext)
    expect(register).toBeCalledWith(username, password, userContext)
  })

  test('returns error if no password found', async () => {
    userContext.mockReturnValue(loginPayload);
    expect.assertions(1);
    try {
      const loggedIn = await login(username, password, userContext)
      return loggedIn
    } catch (error) {
      expect(error.message).toEqual('No user found for username: usersarah')
    }
  })

})