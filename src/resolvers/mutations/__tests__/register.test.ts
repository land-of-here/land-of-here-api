import { register } from '../register'

const username = 'usersarah'
const password = '123magicPassword'
const email = 'sarah@user.com'
const prismaContext = jest.fn()

const registerPayload = {
  token: '123',
  user: {
    username: 'bob',
    handle: 'bob',
    profile: { email: 'bob@email.com' }
  }
}

describe('mutation register', () => {
  test('expects username, password, email arguments', async () => {
    const register = jest.fn()
    register(username, password, email, prismaContext)
    expect(register).toBeCalledWith(username, password, email, prismaContext)
  })

  test('returns object with token and user', async () => {
    prismaContext.mockReturnValue(registerPayload);
    const registered = await register(username, password, email, prismaContext)

    expect(registered).toHaveProperty('token')
    expect(registered).toHaveProperty('user')
  })

})