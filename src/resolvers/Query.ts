import { prismaObjectType} from 'nexus-prisma'

import { me } from './queries'

export const Query = prismaObjectType({
  name: 'Query',
  definition(t) {
    t.field('me', {
      type: 'User',
      resolve: (parent, args, ctx) => me(ctx),
    })
  }
})