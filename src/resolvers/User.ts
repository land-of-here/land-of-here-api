import { prismaObjectType } from 'nexus-prisma'

export const User = prismaObjectType({
  name: 'User',
  definition(t) {
    t.prismaFields([
      'username',
      'handle',
      {
        name: 'profile',
        args: ['where', 'first']
      }
    ])
  },
})