import { Query } from './Query'
import { Mutation } from './Mutation'
import { AuthPayload } from './AuthPayload'
import { User } from './User'
import { UserProfile } from './UserProfile'


export const resolvers = [
  Query,
  Mutation,
  AuthPayload,
  User,
  UserProfile
]

export { Query, Mutation, AuthPayload, User, UserProfile }