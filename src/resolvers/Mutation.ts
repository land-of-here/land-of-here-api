import { prismaObjectType} from 'nexus-prisma'
import { stringArg } from 'nexus'

import { register, login } from './mutations'

export const Mutation = prismaObjectType({
  name: 'Mutation',
  definition(t) {
    t.field('register', {
      type: 'AuthPayload',
      args: { username: stringArg(), password: stringArg(), email: stringArg() },
      resolve: (_, { username, password, email }, ctx) => register(username, password, email, ctx.prisma.createUser),
    })

    t.field('login', {
      type: 'AuthPayload',
      args: { username: stringArg(), password: stringArg() },
      resolve: (_, { username, password }, ctx) => login(username, password, ctx.prisma.user),
    })
  }
})