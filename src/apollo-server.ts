import { createServer } from 'http'
import { ApolloServer } from 'apollo-server-express'
import { applyMiddleware as applyGraphQLMiddleware } from 'graphql-middleware'
import { logger, permissions } from './util'

export default function createApolloServer (
  app,
  schema,
  {
    // Main options
    graphqlEndpoint = '',
    graphqlMiddlewares = [],
    dataSources = () => ({}),
    context = (req: any) => ({ ...req }),
    // Subscriptions
    subscriptionsEndpoint = '',
    wsMiddlewares = [],
    // Mocks
    mocks = {},
    // Apollo Engine
    engineKey = '',
    // HTTP options
    cors = true,
    timeout = 120000,
    // Extra options for Apollo Server
    apolloServerOptions = {}
  }
) {
  // Apollo server options
  const options = {
    ...apolloServerOptions,
    schema: applyGraphQLMiddleware(schema, permissions, ...graphqlMiddlewares),
    tracing: true,
    cacheControl: true,
    introspection: true,
    playground: true,
    engine: engineKey ? { apiKey: engineKey } : false,
    dataSources,
    // Resolvers context in POST requests
    context: context,
    // Resolvers context in WebSocket requests
    subscriptions: {
      path: subscriptionsEndpoint,
      onConnect: async (connection, websocket) => {
        const { authorization, userid } = connection

        let contextData = {}
        try {
          // Simulate `req` object for auth
          const req = { headers: { authorization, userid } }

          // Call all middlewares in order and modify `req`
          await new Promise((resolve, reject) =>
            wsMiddlewares.reduceRight(
              (acc, m) => err => (err ? reject(err) : m(req, null, acc)),
              err => (err ? reject(err) : resolve())
            )()
          )

          contextData = await Object.assign(
            {
              connection,
              websocket,
              request: req,
              req
            },
            connection.context
          )
        } catch (err) {
          logger.error(err)
        }

        return contextData
      }
    }
  }

  // Apollo Server
  const server = new ApolloServer(options)

  // Express middleware
  server.applyMiddleware({
    app,
    cors,
    path: graphqlEndpoint
    // gui: {
    //   endpoint: graphqlEndpoint,
    //   subscriptionEndpoint: graphqlSubscriptionsPath,
    // },
  })

  // Create HTTP server and add subscriptions
  const httpServer = createServer(app)
  httpServer.setTimeout(timeout)
  server.installSubscriptionHandlers(httpServer)

  return httpServer
}
